// Importar el módulo 'express'
const express = require("express");

// Importar las rutas de la API de GitHub
const githubRouter = require("./routes/githubRoutes");

// Crear una instancia de la aplicación Express
const app = express();

// Definir el puerto en el que la aplicación escuchará
const PORT = process.env.PORT || 3000;

// Configurar las rutas de la aplicación
app.use("/api/v1/repositories", githubRouter);

// Iniciar el servidor y escuchar en el puerto especificado
app.listen(PORT, () => {
  console.log(`API is listening on port ${PORT}`);
});
