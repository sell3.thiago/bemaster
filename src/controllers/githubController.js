/**
 * @module githubController
 * @description Controlador para manejar las solicitudes relacionadas con la API de GitHub.
 */

const githubService = require("../services/githubServices");

/**
 * @function getRepositoriesUser
 * @async
 * @description Maneja la solicitud para obtener los repositorios de un usuario en GitHub.
 * @param {Object} req - Objeto de solicitud de Express.
 * @param {Object} res - Objeto de respuesta de Express.
 * @returns {Object} - Respuesta JSON con el estado y los datos de los repositorios o un mensaje de error.
 */
const getRepositoriesUser = async (req, res) => {
  try {
    const username = req.params.username;
    const data = await githubService.getRepositoriesUser(username);

    res.json({ status: 'OK', data: data });
  } catch (error) {
    res.status(500).json({ status: "error", message: "Error on request", error: error.message });
  }
};

module.exports = {
  getRepositoriesUser,
};
