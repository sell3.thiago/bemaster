/**
 * @module githubServices
 * @description Servicios para interactuar con la API de GitHub.
 */

const axios = require("axios");

const GITHUB_API_BASE_URL = "https://api.github.com";

/**
 * @function getRepositoriesUser
 * @async
 * @description Obtiene los repositorios de un usuario en GitHub.
 * @param {string} username - Nombre de usuario de GitHub.
 * @returns {Object} - Respuesta de la API de GitHub o un objeto de error en caso de fallo.
 */
const getRepositoriesUser = async (username) => {
  try {
    const response = await axios.get(
      `${GITHUB_API_BASE_URL}/users/${username}/repos?per_page=10&sort=stars`
    );
    return response.data; // Devolver solo los datos de los repositorios
  } catch (error) {
    // Retornar un objeto de error en lugar de lanzar una excepción
    return {
      error: true,
      message: "Ha ocurrido un error al obtener los repositorios",
    };
  }
};

module.exports = {
  getRepositoriesUser,
};
