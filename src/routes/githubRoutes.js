/**
 * @module githubRoutes
 * @description Definición de las rutas para la API de GitHub.
 */

const express = require("express");
const router = express.Router();

const githubController = require("../controllers/githubController.js");

/**
 * @route GET /api/v1/repositories/:username
 * @description Ruta para obtener los repositorios de un usuario en GitHub.
 * @param {string} :username - Nombre de usuario de GitHub.
 * @returns {Object} - Respuesta según lo manejado por el controlador.
 */
router.get("/:username", githubController.getRepositoriesUser);

module.exports = router;
