# beMaster

- Descargar proyecto
```bash
git clone https://gitlab.com/sell3.thiago/bemaster.git
```

- Acceder al proyecto
```bash
cd bemaster
```

- Instalar dependencias
```bash
npm install
```

- Ejecutar proyecto
```bash
npm run dev
```
---
> Los demás ejercicios están resueltos haciendo uso de issues de gitlab.

## Ejercicio 2
Te presentamos el siguiente fragmento de código:

```javascript
function f(x, y, z) {
 let a = x + y;
 let b = a * z;
 let c = Math.sin(b);
 return c;
}
```

Reemplaza los nombres de las variables con nombres más descriptivos que reflejen mejor
su función
[Aquí la solucion](https://gitlab.com/sell3.thiago/bemaster/-/issues/1)

## Ejercicio 3 - Pensamiento lógico

Escribe una función que tome un número entero como entrada y devuelva un array con
todos los números enteros impares desde 1 hasta el número de entrada. Por ejemplo, si el
número de entrada es 9, la función debería devolver [1, 3, 5, 7, 9].

[Aquí la solución](https://gitlab.com/sell3.thiago/bemaster/-/issues/2)


## Ejercicio 4: Modelado de bases de datos

Imagina que estás construyendo un sistema de gestión de vídeos. Diseña un modelo de
base de datos que incluya tablas para vídeos, autores, colaboradores, comentarios, reviews
y usuarios. Asegúrate de incluir las claves primarias, las claves foráneas y las restricciones
de integridad necesarias para que el sistema funcione correctamente.

[Aquí la solución](https://gitlab.com/sell3.thiago/bemaster/-/issues/3)

## Ejercicio 5: 
Describe cómo estructurarías el backend de una aplicación de comercio electrónico. Habla
sobre las tecnologías que utilizarías, la organización de los archivos, el uso de patrones de
diseño, etc

### Respuesta
Primero que nada, diseñar el backend de una aplicación es una tarea crucial desde su planeación porque basado en esto garantizamos el exito de la aplicación en temas de rendimiento, eficiencia, escalabilidad y mantenibilidad.

- Analizando la situación del ejercicio, se puede diseñar una arquitectura basada en NodeJS utilizando Express como framework web y MongoDB como base de datos NoSQL, esto dado que al manejar grandes volumenes de información, estas bases de datos resultan ser más flexibles para realizar operaciones.
- En temas de seguridad se emplearía JWT la cual suele ser bastante utilizada y con muy buenas referencias.
- Incorporaría un sistema de caching cómo redis para mejorar el rendimiento especialmente en aquellas operaciones que suelen usarse con frecuencia
- Implementar herramientas cómo `Jest` para realizar pruebas unitarias, garantizando así el buen funcionamiento de los servicios
- Manejo de Logs a operaciones que requieran monitoreo
- Implementación usando `ssl`

Es importante que dentro del ciclo de desarrollo cada cosa realizada pueda ser documentada, para esto existen muchas herramientas normalmente me inclinaría por hacerlo con postman, dado que se tienen todas las peticiones ahí, se puede documentar cada endpoint y puede trabajarse en conjunto.

No queda de más indicar manejar lo mejor posible el concepto de `SOLID` dentro del ciclo de desarrollo, así garantizamos buenas prácticas al momento de programar.

## Ejercicio 6: Nomenclatura

Crea un documento de políticas de nomenclatura para el equipo de desarrollo de una
compañía, la política debe incluir nomenclatura de: bases de datos, variables, funciones,
clases, git, etc.

[Aquí la solución](https://gitlab.com/sell3.thiago/bemaster/-/issues/4)